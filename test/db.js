const mongoose = require("mongoose");

var connection = undefined;

exports.connect = done => {
    if (connection === undefined) {
        connection = null;
        mongoose.Promise = global.Promise;
        mongoose.connect(process.env.MONGO_TEST_URI || "mongodb://localhost/360vi-analysis-service-test", {useMongoClient: true}, done);
    } else {
        done();
    }
}
