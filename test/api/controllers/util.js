module.exports = {
    responseMock: function() {
        return {
            statusCode: undefined,
            body: undefined,
            status: function(newStatus) {
                this.statusCode = newStatus;
                return this;
            },
            sendStatus: function(status) {
                this.statusCode = status;
                return this;
            },
            json: function(newBody) {
                this.body = newBody;
                return this;
            },
            location: function(newLocation) {
                this.location = newLocation;
                return this;
            }
        };
    }
};
