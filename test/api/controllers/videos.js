const mocha = require("mocha");
const should = require("should");

const db = require("../../db");
const videos = require("../../../api/controllers/videos");
const Video = require("../../../models/video").Video;
const Algorithm = require("../../../models/algorithm").Algorithm;

const responseMock = require("./util").responseMock;

describe("controllers", () => {
    describe("videos", () => {
        before(done => {
            db.connect(() => {
                Video.remove({}, done);
            });
        });

        describe("postVideoHeader", () => {
            it("adds a new video to the DB and returns it", done => {
                const req = {
                    body: {
                        video_name: "Uusi video",
                        analysis_frameskip: "1",
                        analysis_algorithms: ["recognition"]
                    }
                };

                const resp = responseMock();
                Algorithm.collection.insert([{name: "detection", version: 1}, {name: "recognition", version: 1, dependencies: [{"name": "detection", "minimumVersion": 1, scope: "currentFrame"}]}])
                .then(() => videos.postVideoHeader(req, resp, null))
                .then(res => {
                    res.statusCode.should.eql(202);
                    resp.body.data.attributes.name.should.eql(req.body.video_name);
                    resp.body.data.relationships.analysisResults.data.should.eql([{"algorithmName": "detection"}, {"algorithmName": "recognition"}]);

                    return Video.find();
                })
                .then(docs => {
                    docs.length.should.eql(1);
                    docs[0].name.should.eql(req.body.video_name);
                    docs[0].frameSkip.should.eql(parseInt(req.body.analysis_frameskip));
                    docs[0].analysisRuns.length.should.eql(2);
                    done();
                });
            });
        });

        describe("postVideoData", () => {
            it("does not allow continuing a finished upload", done => {
                var req = {headers: {"content-type": "video/mp4"}, accepts: () => "json"};
                new Video({status: "processing"}).save()
                .then(doc => {
                    req.url = `http://example.com/videos/${doc.id}`;
                    return videos.postVideoData(req, responseMock());
                })
                .then(res => {
                    res.statusCode.should.eql(405);
                    done();
                })
            });

            it("does not allow uploading to a video from a DASH source", done => {
                var req = {headers: {"content-type": "video/mp4"}, accepts: () => "json"};
                new Video({status: "pending", dashSource: "http://example.com/dash.mpd"}).save()
                .then(doc => {
                    req.url = `http://example.com/videos/${doc.id}`;
                    return videos.postVideoData(req, responseMock());
                }).then(res => {
                    res.statusCode.should.eql(422);
                    done();
                })
            });
        });
    });
});