const mocha = require("mocha");
const should = require("should");

const db = require("../../db");
const algorithms = require("../../../api/controllers/algorithms");
const Algorithm = require("../../../models/algorithm").Algorithm;

const responseMock = require("./util").responseMock;

function request(body) {
    return {body: body, header: () => undefined, connection: {remoteAddress: "::ffff:127.0.0.1"}};
}

describe("controllers", () => {
    describe("algorithms", () => {
        before(done => {
            db.connect(() => {
                Algorithm.remove({}, done);
            });
        });

        describe("registerAlgorithm", () => {
            it("adds a new algorithm to the DB and returns it", done => {
                const req = request({
                    name: "face-detection",
                    version: 1,
                    current_frame_dependencies: []
                });

                const resp = responseMock();
                algorithms.registerAlgorithm(req, resp, null)
                .then(res => {
                    res.statusCode.should.eql(201);
                    resp.body.data.attributes.name.should.eql(req.body.name);
                    resp.body.data.attributes.version.should.eql(req.body.version);
                    resp.body.data.attributes.dependencies.length.should.eql(req.body.current_frame_dependencies.length);

                    return Algorithm.find();
                })
                .then(docs => {
                    docs.length.should.eql(1);
                    docs[0].name.should.eql(req.body.name);
                    docs[0].version.should.eql(req.body.version);
                    docs[0].dependencies.length.should.eql(req.body.current_frame_dependencies.length);
                    done();
                });
            });

            it("adds algorithm with newer version", done => {
                const req = request({
                    name: "eye-detection",
                    version: 5,
                    current_frame_dependencies: ["face-detection:1"]
                });
                new Algorithm({name: "eye-detection", version: 4, host: "127.0.0.1", port: 80, dependencies: []}).save()
                .then(doc => {
                    const resp = responseMock();
                    return algorithms.registerAlgorithm(req, resp, null)
                }).then(res => {
                    res.statusCode.should.eql(201);
                    res.body.data.attributes.name.should.eql(req.body.name);
                    res.body.data.attributes.version.should.eql(req.body.version);
                    res.body.data.attributes.dependencies.should.eql([{name: "face-detection", minimumVersion: 1, scope: "currentFrame"}]);

                    return Algorithm.find({name: req.body.name});
                })
                .then(docs => {
                    docs[0].version.should.eql(4);
                    docs[1].name.should.eql(req.body.name);
                    docs[1].version.should.eql(req.body.version);
                    docs[1].dependencies[0]._doc.should.eql({name: "face-detection", minimumVersion: 1, scope: "currentFrame"});
                    done();
                });
            });

            it("rejects duplicate algorithm", done => {
                const req = request({
                    name: "abc",
                    version: 1,
                    whole_video_dependencies: ["def:10"]
                });
                new Algorithm({name: "abc", version: "1", host: "127.0.0.1", port: 80}).save()
                .then(doc => {
                    const resp = responseMock();
                    return algorithms.registerAlgorithm(req, resp, null);
                }).then(res => {
                    res.statusCode.should.eql(409);

                    return Algorithm.findOne({name: "abc"});
                })
                .then(doc => {
                    doc.version.should.eql(1);
                    doc.dependencies.length.should.eql(0);
                    done();
                });
            });

            it("allows host/port change without version bump", done => {
                const req = request({
                    name: "to_change",
                    version: 1,
                    host: "example.com:3000"
                });
                new Algorithm({name: "to_change", version: "1", host: "example.com", port: "3001"}).save()
                .then(doc => {
                    const resp = responseMock();
                    return algorithms.registerAlgorithm(req, resp, null);
                }).then(res => {
                    res.statusCode.should.eql(200);
                    return Algorithm.findOne({name: "to_change"});
                })
                .then(doc => {
                    doc.version.should.eql(1);
                    doc.host.should.eql(req.body.host);
                    done();
                });
            });
        });
    });
});