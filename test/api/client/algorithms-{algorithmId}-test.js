'use strict';
const chai = require('chai');
const ZSchema = require('z-schema');

const validator = new ZSchema({});
const supertest = require('supertest');
const api = supertest('http://localhost:3000'); // supertest init;
const expect = chai.expect;

describe('/algorithms/{algorithmId}', function() {
  describe('get', function() {
    it('should respond with 404 Analysis not found', function(done) {
      api.get('/algorithms/abcdef1234567890abcdef12')
      .expect(404)
      .end(function(err, res) {
        if (err) return done(err);

        done();
      });
    });
  });

});
