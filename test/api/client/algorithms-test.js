'use strict';
const chai = require('chai');
const ZSchema = require('z-schema');

const validator = new ZSchema({});
const supertest = require('supertest');
const api = supertest('http://localhost:3000'); // supertest init;
const expect = chai.expect;

describe('/algorithms', function() {
  describe('post', function() {
    it('should respond with 422 Unknown dependency', function(done) {
      api.post('/algorithms')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send("name=eyedetection")
      .send("version=150")
      .send("current_frame_dependencies=facedetection%3A20")
      .send("current_frame_dependencies=nonexistent_algorithm%3A10")
      .send("whole_video_dependencies=another_nonexistent_algorithm%3A10")
      .expect(422)
      .end(function(err, res) {
        if (err) return done(err);

        expect(res.body.errors.length).to.equal(1);
        expect(res.body.errors[0].code).to.equal("UNMET_DEPENDENCY");
        done();
      });
    });
  });

  describe('get', function() {
    it('should respond with 200 Success', function(done) {
      /*eslint-disable*/
      const schema = {
        "type": "object",
        "required": [
          "data"
        ],
        "properties": {
          "data": {
            "type": "array",
            "items": {
              "allOf": [
                {
                  "type": "object",
                  "required": [
                    "id",
                    "type"
                  ],
                  "properties": {
                    "id": {
                      "type": "string",
                      "pattern": "^[a-f0-9]{24}$"
                    },
                    "type": {
                      "type": "string",
                      "enum": [
                        "algorithms"
                      ]
                    }
                  }
                },
                {
                  "type": "object",
                  "required": [
                    "attributes"
                  ],
                  "properties": {
                    "attributes": {
                      "type": "object",
                      "required": [
                        "name",
                        "version"
                      ],
                      "properties": {
                        "name": {
                          "type": "string",
                          "pattern": "^[a-zA-Z0-9_-]+$"
                        },
                        "version": {
                          "type": "integer"
                        },
                        "dependencies": {
                          "type": "array",
                          "items": {
                            "type": "object",
                            "required": [
                              "name",
                              "minimumVersion",
                              "scope"
                            ],
                            "properties": {
                              "name": {
                                "type": "string",
                                "pattern": "^[a-zA-Z0-9_-]+$"
                              },
                              "minimumVersion": {
                                "type": "integer"
                              },
                              "scope": {
                                "type": "string",
                                "enum": [
                                  "currentFrame",
                                  "wholeVideo"
                                ]
                              }
                            }
                          }
                        }
                      }
                    },
                    "links": {
                      "type": "object",
                      "required": [
                        "self"
                      ],
                      "properties": {
                        "self": {
                          "type": "string",
                          "format": "uri"
                        }
                      }
                    }
                  }
                }
              ]
            }
          }
        }
      };

      /*eslint-enable*/
      api.get('/algorithms')
      .set('Content-Type', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);

        expect(validator.validate(res.body, schema)).to.be.true;
        done();
      });
    });

  });

});
