'use strict';
var chai = require('chai');
var supertest = require('supertest');
var api = supertest('http://localhost:3000'); // supertest init;
var expect = chai.expect;

describe('/videos/{videoId}/manifest', function() {
  describe('get', function() {
    it('should respond with 404 Video not found', function(done) {
      api.get('/videos/abcdef1234567890abcdef12/manifest')
      .set('Content-Type', 'application/json')
      .expect(404)
      .end(function(err, res) {
        if (err) return done(err);

        expect(res.body).to.equal(null); // non-json response or no schema
        done();
      });
    });

  });

});
