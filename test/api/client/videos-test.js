'use strict';
const chai = require('chai');
const ZSchema = require('z-schema');

const validator = new ZSchema({});
const supertest = require('supertest');
const api = supertest('http://localhost:3000'); // supertest init;
const expect = chai.expect;

describe('/videos', function() {
  describe('post', function() {
    it('should respond with 202 Video header accepted and...', function(done) {
      /*eslint-disable*/
      const schema = {
        "type": "object",
        "required": [
          "data"
        ],
        "properties": {
          "data": {
            "allOf": [
              {
                "type": "object",
                "required": [
                  "id",
                  "type"
                ],
                "properties": {
                  "id": {
                    "type": "string",
                    "pattern": "^[a-f0-9]{24}$"
                  },
                  "type": {
                    "type": "string",
                    "enum": [
                      "videos"
                    ]
                  }
                }
              },
              {
                "type": "object",
                "required": [
                  "attributes",
                  "links"
                ],
                "properties": {
                  "attributes": {
                    "type": "object",
                    "required": [
                      "status"
                    ],
                    "properties": {
                      "name": {
                        "type": "string"
                      },
                      "status": {
                        "type": "string",
                        "enum": [
                          "pending",
                          "receiving",
                          "processing",
                          "done"
                        ]
                      }
                    }
                  },
                  "links": {
                    "type": "object",
                    "required": [
                      "self"
                    ],
                    "properties": {
                      "self": {
                        "type": "string",
                        "format": "uri"
                      }
                    }
                  },
                  "relationships": {
                    "type": "object",
                    "properties": {
                      "testAlgorithmResult": {
                        "type": "object",
                        "required": [
                          "links",
                          "data"
                        ],
                        "properties": {
                          "links": {
                            "type": "object",
                            "required": [
                              "related"
                            ],
                            "properties": {
                              "related": {
                                "type": "string",
                                "format": "uri"
                              }
                            }
                          },
                          "data": {
                            "type": "object",
                            "required": [
                              "id",
                              "type"
                            ],
                            "properties": {
                              "id": {
                                "type": "string",
                                "pattern": "^[a-f0-9]{24}$"
                              },
                              "type": {
                                "type": "string",
                                "enum": [
                                  "analyses"
                                ]
                              }
                            }
                          }
                        }
                      },
                    }
                  }
                }
              }
            ]
          }
        }
      };

      /*eslint-enable*/
      api.post('/videos')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({
          video_name: `Video from testing on ${(new Date()).toISOString()}`,
          analysis_frameskip: 2,
          analysis_algorithms: "detection"
      })
      .expect(202)
      .expect('Location', /\/videos\/[a-f0-9]{24}/)
      .end(function(err, res) {
        if (err) return done(err);

        expect(validator.validate(res.body, schema)).to.be.true;
        api.get('/videos/' + res.body.data.id)
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);
          done();
        })
      });
    });

    it('should respond with 400 Bad request', function(done) {
      api.post('/videos')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({
          video_name: `Video from testing on ${(new Date()).toISOString()}`,
          analysis_frameskip: "abc",
          analysis_algorithms: "detection"
      })
      .expect(400)
      .end(function(err, res) {
        if (err) return done(err);

        expect(res.body.errors.length).to.equal(1); // non-json response or no schema
        expect(res.body.errors[0].code).to.equal("INVALID_REQUEST_PARAMETER");
        done();
      });
    })

  });

});
