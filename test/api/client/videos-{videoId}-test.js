'use strict';
var chai = require('chai');

var supertest = require('supertest');
var api = supertest('http://localhost:3000'); // supertest init;
var expect = chai.expect;

describe('/videos/{videoId}', function() {
  describe('get', function() {
    it('should respond with 400 Invalid ID supplied', function(done) {
      api.get('/videos/abc123')
      .set('Content-Type', 'application/json')
      .expect(400)
      .end(function(err, res) {
        if (err) return done(err);

        expect(res.body.errors.length).to.equal(1);
        expect(res.body.errors[0].code).to.equal("INVALID_REQUEST_PARAMETER");
        done();
      });
    });

    it('should respond with 404 video not found', function(done) {
      api.get('/videos/abcdef1234567890abcdef12')
      .set('Content-Type', 'video/mp4')
      .expect(404)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
    });

  });

  describe('post', function() {
      it('should respond with 400 Invalid ID supplied', function(done) {
      api.post('/videos/abc123')
      .set('Content-Type', 'video/mp4')
      .expect(400)
      .end(function(err, res) {
        if (err) return done(err);

        expect(res.body.errors.length).to.equal(1);
        expect(res.body.errors[0].code).to.equal("INVALID_REQUEST_PARAMETER");
        done();
      });
    });

    it('should respond with 404 analysis not found', function(done) {
      api.post('/videos/abcdef1234567890abcdef12')
      .set('Content-Type', 'video/MP2T')
      .expect(404)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
    });
  })

});
