# API and platform for 360VI analysis execution

This is a Node.js service, which:
1. integrates different analysis algorithms, handling their dependencies
1. provides analyses as an HTTP service

The motivation for this platform is that different algorithms live in separate codebases and may have different distribution and library requirements.


## Overview

Apart from the public HTTP API, there are also internal HTTP APIs for communication between the platform and various analysis programs.

A typical flow of the service would be:
1. the platform is started
1. algorithm programs are started, and they register themselves (`POST /algorithms` to the platform, see API documentation)
1. a client requests the analysis of a video (`POST /videos` to the platform, see API documentation)
1. the platform requests individual analyzers to analyze frames (`POST /analyze` to each analysis program)
1. the platform provides the results to the client

The API is documented with Swagger and the documentation exposed at `/swagger`.


## Deployment requirements

The platform uses MongoDB for persistence. The environment variable `MONGO_URI` is used (defalting to `mongodb://localhost/360vi-analysis-service`).

It is assumed that the platform and individual algorithm programs are on the same computer for the sake of performance. They must have a data exchange directory available; it is defined for the platform with the environment variable `DATA_EXCHANGE_DIRECTORY` (defaults to `/tmp/360vi-analysis`). The `/tmp` of the host computer should reside on RAM to eliminate disk I/O of uncompressed frames.

To achieve different software environments on the same host, Docker can be used.


## Deployment with Docker

See the Docker [Get Started](https://docs.docker.com/get-started/) guide parts 1–2 if you are not familiar with Docker.

1. Add a network to be used by the platform and algorithm containers (see [Docker container networking](https://docs.docker.com/engine/userguide/networking/))
1. Start a `mongo` container (the `Dockerfile` defines the address to use as `mongodb`) in the network
1. Start the `360vi-platform` container (the `algorithm-server` analyzer wrapper defaults to connecting to `analysis-service`, so that is probably the best name to use) in the network, linking a host directory to `DATA_EXCHANGE_DIRECTORY` (see [Volumes](https://docs.docker.com/engine/admin/volumes/volumes/)) and mapping a suitable host port to 80 on the container.
1. Start the individual analysis programs, using the same data exchange directory mapping. Note that if the analyses need CUDA, you should use [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) instead of the regular `docker` command.