import { Schema, Types, Document, model } from "mongoose";

import { IAlgorithmDocument } from "./algorithm";

export type VideoStatus =
    "pending"
    | "receiving"
    | "processing"
    | "done";

export interface IVideoDocument extends Document {
    id: string;
    name: string;
    status: VideoStatus;
    dashSource: String;
    analysisRuns: {
        algorithm: IAlgorithmDocument;
        analysis: Types.ObjectId;
    }[];
    frameSkip: number;
}

const videoSchema = new Schema({
    name: String,
    status: {type: String, required: true, enum: ["pending", "receiving", "processing", "done"]},
    dashSource: String,
    analysisRuns: [{
        _id: false,
        algorithm: {type: Schema.Types.ObjectId, ref: "Algorithm", required: true},
        analysis: {type: Schema.Types.ObjectId, ref: "Analysis"}

    }],
    frameSkip: Number
}, {
    toObject: {transform: (doc, ret) => {
        delete ret._id;
        return ret;
    }}
});

export const Video = model<IVideoDocument>("Video", videoSchema);