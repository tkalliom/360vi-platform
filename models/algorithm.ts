import { Schema, Document, model } from "mongoose";

export type AlgorithmDependencyScope =
    "currentFrame"
    | "wholeVideo";

export interface IAlgorithmDocument extends Document {
    id: string;
    name: string;
    version: number;
    host: string;
    port: number;
    dependencies: {
        name: string;
        minimumVersion: number;
        scope: AlgorithmDependencyScope;
    }[];
}

const algorithmSchema = new Schema({
    name: {type: String, required: true},
    version: {type: Number, required: true},
    host: {type: String, required: true},
    port: {type: Number, required: true},
    dependencies: [{
        _id: false,
        name: {type: String, required: true},
        minimumVersion: {type: Number, required: true},
        scope: {type: String, required: true, enum: ["currentFrame", "wholeVideo"]}
    }]
}, {
    toObject: {transform: (doc, ret) => {
        delete ret._id;
        return ret;
    }}
});
algorithmSchema.index({name: 1, version: 1}, {unique: true});

export const Algorithm = model<IAlgorithmDocument>("Algorithm", algorithmSchema);
