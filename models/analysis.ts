import { Schema, Types, Document, model } from "mongoose";

export interface IAnalysisDocument extends Document {
    id: string;
    algorithm: Types.ObjectId;
    video: Types.ObjectId;
    results: {
        timestamp: number;
        score: number;
        rect: {
            x: number;
            y: number;
            width: number;
            height: number;
        };
        classification: {
            vocabulary: string;
            label: string;
        }
    }[]
}

const analysisSchema = new Schema({
    algorithm: {type: Schema.Types.ObjectId, ref: "Algorithm", required: true},
    video: {type: Schema.Types.ObjectId, ref: "Video", required: true},
    results: {
        type: [{
            _id: false,
            timestamp: Number,
            score: Number,
            rect: {
                x: Number,
                y: Number,
                width: Number,
                height: Number
            },
            classification: {
                vocabulary: String,
                label: String
            }
        }],
        required: true
    }
}, {
    timestamps: true,
    toObject: {transform: (doc, ret) => {
        delete ret._id;
        return ret;
    }}
});

export const Analysis = model<IAnalysisDocument>("Analysis", analysisSchema);