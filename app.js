"use strict";
const express = require("express");
const swaggerExpress = require("swagger-express-mw");
const logger = require("morgan");
const mongoose = require("mongoose");
const cors = require("cors");

class VideoServiceApplication {
    constructor() {
        this.app = express();
        this.app.use(logger("dev"));
        this.app.use(cors());
        mongoose.Promise = global.Promise;
        mongoose.connect(process.env.MONGO_URI || "mongodb://localhost/360vi-analysis-service", {useMongoClient: true})
            .catch(err => { throw err; });
        swaggerExpress.create({ appRoot: __dirname }, (err, runner) => {
            if (err) {
                throw err;
            }
            runner.register(this.app);
        });
    }
}
const videoServiceApp = new VideoServiceApplication();
const expressApp = videoServiceApp.app;

module.exports = expressApp;
