function pad(num: number, width: number) {
    let str = "" + num;
    while (str.length < width) {
        str = "0" + str;
    }
    return str;
}

function timeString(timeMs: number) {
    const hours = Math.floor(timeMs / 3600000);
    const minutes = Math.floor(timeMs % 3600000 / 60000);
    const seconds = Math.floor(timeMs % 60000 / 1000);
    const milliseconds = timeMs % 1000;

    return `${pad(hours, 2)}:${pad(minutes, 2)}:${pad(seconds, 2)},${pad(milliseconds, 3)}`;
}

class Line {
    startMs: number;
    endMs: number;
    content: string;

    toString() {
        return `${timeString(this.startMs)} --> ${timeString(this.endMs)}\n${this.content}`;
    }
}

export class Srt {
    lines: Line[];

    constructor() {
        this.lines = new Array<Line>();
    }

    addLine(content: string, startPts: number, timeBase: number) {
        let line = new Line();
        line.content = content;
        line.startMs = Math.round(startPts / timeBase * 1000);
        line.endMs = Math.round(startPts / timeBase * 1000 + 33);
        this.lines.push(line);
    }

    toString() {
        return this.lines.sort((a, b) => a.startMs - b.startMs).map((line, index) => `${index + 1}\n${line.toString()}`).join("\n\n") + "\n";
    }
}