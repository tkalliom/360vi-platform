import {Types} from "mongoose";

import {IAlgorithmDocument, Algorithm} from "../../models/algorithm"

interface DependencyNode {
    algorithm: IAlgorithmDocument;
    mark: "none" | "temporary" | "permanent";
}

// Assumes a complete list of dependencies
function sort(list: IAlgorithmDocument[]) {
    const ret: IAlgorithmDocument[][] = [[]];

    const added = (name: string) => ret.some(pass => !!pass.find(a => a.name === name));
    const addedPreviously = (name: string) => ret.slice(0, -1).some(pass => !!pass.find(a => a.name === name));
    const numAdded = () => ret.map(pass => pass.length).reduce((acc, pass) => acc + pass, 0);

    Array.prototype.push.apply(ret[0], list.filter(alg => !alg.dependencies || !alg.dependencies.length));

    while (numAdded() < list.length) {
        const inCurrentPass = (a: IAlgorithmDocument) => !added(a.name) && !a.dependencies.some(dep => !added(dep.name) || (dep.scope === "wholeVideo" && !addedPreviously(dep.name)));
        while (list.some(a => inCurrentPass(a))) {
            Array.prototype.push.apply(ret[ret.length - 1], list.filter(a => inCurrentPass(a)));
        }
        const inNextPass = (a: IAlgorithmDocument) => !added(a.name) && !a.dependencies.some(dep => !added(dep.name));
        if (list.some(a => inNextPass(a))) {
            ret.push(list.filter(a => inNextPass(a)));
        }
    }

    return ret;
}

function doBuildDependencies(analysesToRun: IAlgorithmDocument[]): Promise<IAlgorithmDocument[]> {
    // We check at registration phase that version dependencies are satisfied, so here it's enough to lookup by name

    const missingFromList: string[] = [];
    for (const analysis of analysesToRun) {
        if (!analysis.dependencies) {
            continue;
        }
        for (const dependency of analysis.dependencies) {
            if (!analysesToRun.some(alg => alg.name === dependency.name)) {
                missingFromList.push(dependency.name);
            }
        }
    }

    if (missingFromList.length) {
        return Algorithm.aggregate([
            {$match: {name: {$in: missingFromList}}},
            {$sort: {version: -1}},
            {$group: {_id: "$name", id: {$first: "$_id"}, name: {$first: "$name"}, dependencies: {$first: "$dependencies"}}}
        ])
        .then(docs => doBuildDependencies(analysesToRun.concat(docs as IAlgorithmDocument[])))
    } else {
        return Promise.resolve(analysesToRun);
    }
}

export function buildDependencies(analysesToRun: IAlgorithmDocument[]) {
    return doBuildDependencies(analysesToRun)
    .then(list => sort(list));
}

// Assumes a section of an array returned by sort()
export function firstSinglePassSegment(analyses: IAlgorithmDocument[]) {
    let cfEntered = false;
    let firstOfNextPass = -1;

    let i: number = undefined;
    for (i = 0; i < analyses.length; ++i) {
        if (!analyses[i].dependencies.some(dep => dep.scope === "wholeVideo")) {
            cfEntered = true;
        }
        else if (cfEntered) {
            firstOfNextPass = i;
            break;
        }
    }

    return analyses.slice(0, i);
}