import {spawn} from "child_process";
import {createInterface} from "readline";
import {writeFile} from "fs";

const input = process.argv[2];
const frameSkip = parseInt(process.argv[3]);
const directoryPath = process.argv[4];
/* fluent-ffmpeg does not expose FFmpeg stdout directly,
    so avoid the PassThrough overhead by handling the process ourselves when handling raw video */
const ffmpeg = spawn('ffmpeg', [
    "-copyts",
    "-debug_ts",
    "-i", input,
    "-f", "image2pipe",
    "-vcodec", "rawvideo",
    "-vf", `select='not(mod(n\,${frameSkip}))'`,
    "-pix_fmt", "bgr24", // TODO: evaluate format conversion cost vs. data transfer vs. algorithm needs
    "-vsync", "passthrough",
    "-"
]);
if (input === "pipe:0") {
  process.stdin.pipe(ffmpeg.stdin);
}

let frameSize = undefined;
let ptss: number[] = [];
let writtenFrames = 0;

let matches = undefined;
let frameIndex = 0;
createInterface(ffmpeg.stderr) // FFMpeg outputs result video data to stdout and other messages to stderr
.on("line", line => {
    if (!frameSize && line.startsWith("    Stream #0:") && (matches = line.match(/Video\: (.*)/))) {
        const items = line.split(/, (?![^(]*\))/);
        const frameDescription = items[2];
        const width = parseInt(frameDescription.split("x")[0]);
        const height = parseInt(frameDescription.split("x")[1].split(" ")[0]);
        frameSize = width * height * 3; // 24bpp == 3Bpp
        // TODO: Black-and-white/10bit/etc...

        const tbnString = items[6];
        const tbn = parseInt(tbnString.split(" ")[0].replace("k", "000"));

        process.stdout.write(`${width}\t${height}\t${tbn}\n`);
    } else if (matches = line.match(/^decoder .+[^a-z_]frame_pts: ?([0-9]+)/)) { // -debug_ts form not guaranteed to hold between versions
        if (frameIndex % frameSkip == 0) {
            // The order of data and info messages is not guaranteed, and sometimes one might get ahead the other?
            ptss.push(matches[1]);
            if (writtenFrames >= ptss.length) {
                process.stdout.write(`${ptss.length -1}\n`);
            }
        }
        ++frameIndex;
    }
});

let frame: Buffer = undefined;
let outputFrameIndex = -1;
ffmpeg.stdout.on("readable", () => {
    if (!frameSize) {
        console.error("Getting frame data before codec data?");
        return;
    }

    // You would think that the less time you spent idling, the faster you finish...
    // However, turns out thet syscalls are so expensive it is _significantly_ faster (150% compared to 16KB chunks) to let bytes buffer up until we can write a whole frame
    // On 3840x1920 video with 21MB frames, this strategy gives us a runtime of 18 seconds vs. the 17 of ffmpeg > /tmp
    while (frame = ffmpeg.stdout.read(frameSize)) {
        const currentFrameIndex = ++outputFrameIndex;
        writeFile(`${directoryPath}/${currentFrameIndex}.dat`, frame, err => {
            if (err) {
                process.exit(1);
            } else {
                ++writtenFrames;
                if (ptss.length >= writtenFrames) {
                    process.stdout.write(`${ptss[currentFrameIndex]}\n`);
                }
            }
        });
        frame = undefined;
    }
});
