import {Request, Response, NextFunction} from "express";
import {ParameterValue} from "sway";

import {parse} from "url";
import {createWriteStream, watch, unlink, readFile, writeFile, createReadStream} from "fs";
import mkdirp = require("mkdirp");
import {fork} from "child_process";
import {createInterface} from "readline";
import {PassThrough, Readable} from "stream";
import {FfmpegCommand} from "fluent-ffmpeg";
import ffmpeg = require("fluent-ffmpeg");
import {request} from "http";
import debuglogger = require("debug");

import {IVideoDocument, Video, VideoStatus} from "../../models/video";
import {IAnalysisDocument, Analysis} from "../../models/analysis"
import {IAlgorithmDocument, Algorithm} from "../../models/algorithm";
import {Srt} from "../helpers/srt";
import {buildDependencies, firstSinglePassSegment} from "../helpers/dependencies";
import analyses = require("./analyses");

const debug = debuglogger("360vi-platform")

interface FrameResultMap {
    [algorithm: string]: object[];
}

function location(video: IVideoDocument) {
    if (!video) {
        throw new RangeError("Trying to construct location of empty");
    } else if (!video.id) {
        throw new RangeError("Trying to construct location of object without an id");
    } else {
        return `/videos/${video.id}`;
    }
}

function responseObject(video: IVideoDocument) {
    return {
        "id": video.id,
        "type": "videos",
        "attributes": {
            "name": video.name,
            "status": video.status,
            "dashSource": video.dashSource
        },
        "links": {
            "self": location(video)
        },
        "relationships": {
            "analysisResults": {
                "data": video.analysisRuns.map(run => {
                    const ret = {algorithmName: run.algorithm.name};
                    if (run.analysis) {
                        ret["type"] = "analyses";
                        ret["id"] = run.analysis;
                    }
                    return ret;
                })
            }
        }
    };
}

function exchangeDirectory() {
    return "DATA_EXCHANGE_DIRECTORY" in process.env ? process.env.DATA_EXCHANGE_DIRECTORY : "/tmp/360vi-analysis";
}

function postVideoHeader(req: Request, res: Response, next: NextFunction) {
    if (typeof req.body.analysis_algorithms === "string") {
        req.body.analysis_algorithms = [req.body.analysis_algorithms];
    }
    const videoParameters: {name: string, status: VideoStatus, dashSource: String, analysisRuns: {algorithm: IAlgorithmDocument}[], frameSkip: number} = {
        name: req.body.video_name,
        status: "pending",
        dashSource: req.body.video_source,
        analysisRuns: [],
        frameSkip: parseInt(req.body.analysis_frameskip)
    };

    return Algorithm.aggregate([
        {$match: {name: {$in: req.body.analysis_algorithms}}},
        {$sort: {version: -1}},
        {$group: {_id: "$name", id: {$first: "$_id"}, name: {$first: "$name"}, dependencies: {$first: "$dependencies"}}}
    ])
    .then(aggregates => {
        for (const algorithm of req.body.analysis_algorithms) {
            if (!aggregates.some(aggregate => aggregate["_id"] === algorithm)) {
                return Promise.reject({status: 422, code: "UNMET_DEPENDENCY", detail: `Unknown algorithm ${algorithm}`})
            }
        }
        return buildDependencies(aggregates as IAlgorithmDocument[]);
    })
    .then(dependencies => {
        const x = Object.assign(videoParameters, {analysisRuns: [].concat.apply([], dependencies.map(pass => pass.map(dep => {return {algorithm: dep.id}})))});
        return new Video(x).save();
    })
    .then(doc => Video.findById(doc.id).populate("analysisRuns.algorithm"))
    .then(doc => res.status(202).location(location(doc)).json({data: responseObject(doc)}))
    .catch(err => {
        return res.status(err.status || 500).json({"errors": [{"code": err.code || "DATABASE_ERROR", detail: err.detail || err.message || err}]})
    });
    // TODO: if there was a DASH source, start downloading and processing
}

function getVideoById(req: Request, res: Response, next: NextFunction) {
    const idParam: ParameterValue = req["swagger"].params.videoId;
    return Video.findById(idParam.value).populate("analysisRuns.algorithm").then(
      doc => doc ? res.status(200).json({data: responseObject(doc)}) : res.sendStatus(404),
      err => res.status(500).json({"errors": [{"code": "DATABASE_ERROR", "detail": err}]})
    );
}

function getVideoManifest(req: Request, res: Response, next: NextFunction) {
    res.sendStatus(501);
}

function postVideoData(req: Request, res: Response, next: NextFunction) {
    // Swagger param parsing would also parse the body, which we don't want
    const id = parse(req.url).pathname.split("/").pop();
    const acceptedType = req.accepts("json", "mp4", "MP2T") as "json" | "mp4" | "MP2T" | false;
    if (!/^[a-f0-9]{24}$/.test(id)) {
        return Promise.resolve(res.status(400).json({errors: [{code: "INVALID_REQUEST_PARAMETER", in: "path", name: "videoId"}]}));
    } else if (["video/MP2T", "video/mp4"].indexOf(req.headers["content-type"]) < 0) {
        return Promise.resolve(res.status(415).json({errors: [{code: "INVALID_CONTENT_TYPE"}]}));
    } else if (!acceptedType) {
        return Promise.resolve(res.status(406).json());
    }
    const chunkSuffix = req.headers["Chunk-Number"] ? req.headers["Chunk-Number"] as string : "";
    let inputVideoFilePath: string = undefined;
    let header: IVideoDocument = undefined;

    debug(`Handling a request for ${chunkSuffix ? "chunk " + chunkSuffix : "entire video"}`);
    return Video.findById(id).populate("analysisRuns.algorithm")
    .catch<IVideoDocument>(err => Promise.reject({detail: err}))
    .then(doc => {
        if (!doc) {
            return Promise.reject({status: 404});
        } else if (doc.status !== "pending") {
            return Promise.reject({status: 405});
        }
        debug(`Found a header with ID ${id} and it specifies algorithms ${doc.analysisRuns.map(ar => ar.algorithm.name)}`);
        if (["video/mp4", "video/MP2T"].indexOf(req.headers["content-type"]) < 0) {
            if (req.headers["content-length"] === "0") {
                return Promise.reject({status: 501}); // Finishing chunk upload
            } else {
                return Promise.reject({status: 415});
            }
        } else if (doc.dashSource) {
            return Promise.reject({status: 422});
        }
        doc.status = "receiving";
        return doc.save();
    })
    .then(doc => new Promise<void>((resolve, reject) => {
        header = doc;
        mkdirp(`${exchangeDirectory()}/${doc.id}`, err => {
            if (err) {
                reject({detail: err});
            } else {
                resolve();
            }
        });
    }))
    .then(() => {
        req.on("finish", () => {
            header.status = "processing";
            header.save();
        });
        if (req.headers["content-type"] === "video/MP2T" && !header.analysisRuns.some(run => run.algorithm.dependencies.some(dep => dep.scope !== "currentFrame")) && acceptedType === "json") {
            debug(`Handling a TS input, with no need for multiple passes, and outputting JSON, so not storing input on disk`);
            return Promise.resolve(req);
        } else if (req.headers["chunk-number"] && header.analysisRuns.some(run => run.algorithm.dependencies.some(dep => dep.scope !== "currentFrame"))) {
            // TODO a mechanism to allow multiple passes over several chunks
        } else {
            inputVideoFilePath = `${exchangeDirectory()}/${id}/${chunkSuffix}input.${req.headers["content-type"] === "video/mp4" ? "mp4" : "ts"}`
            return runInputViaDisk(req, inputVideoFilePath, chunkSuffix);
        }
    })
    .then(source => doAnalysisPass(source, id, chunkSuffix, header.frameSkip, header.analysisRuns.map(run => run.algorithm), header.analysisRuns.map(run => run.algorithm), undefined))
    .then(resultTuple => acceptedType === "json" ? buildAnalysisResponse(header, resultTuple[0]) : buildVideoResponse(id, inputVideoFilePath, acceptedType, resultTuple[0], resultTuple[1]))
    .then(response => {
        header.status = req.headers["Chunk-Number"] ? "pending" : "done";
        header.save();
        if (response) {
            if (acceptedType === "json") {
                res.json({data: response.map(a => analyses.responseObject(a))}).send();
            } else if ((typeof response) === "string") {
                createReadStream(response as string)
                .pipe(res);
            } else {
                (response as FfmpegCommand)
                .pipe(res);
            }
        } else {
            res.status(204).send();
        }
    })
    .catch(err => "status" in err ? res.sendStatus(err.status) : res.status(500).json({errors: [{detail: err.detail || err}]}));

}

function runInputViaDisk(req: Request, inputVideoFilePath: string, chunkSuffix: string): Promise<string | Readable> {
    const inputVideoFile = createWriteStream(inputVideoFilePath);

    if (req.headers["content-type"] === "video/MP2T") {
        debug("Will decode request payload on-the-fly while saving a copy for remuxing");
        const inputVideoStream = new PassThrough();
        req.on("data", chunk => {
            inputVideoFile.write(chunk);
            inputVideoStream.write(chunk);
        });
        return Promise.resolve(inputVideoStream);
    } else {
        debug("Will receive whole request payload before decoding since moov might be at the end");
        req.pipe(inputVideoFile);

        return new Promise((resolve, reject) => {
            inputVideoFile.on("finish", () => {
                resolve(inputVideoFilePath);
            }).on("error", err => {
                reject({detail: err});
            });
        })
    }
}

function doAnalysisPass(source: string | Readable, id: string, chunkSuffix: string, frameSkip: number, allAlgorithms: IAlgorithmDocument[], remainingAlgorithms: IAlgorithmDocument[], resultSet: FrameResultMap): Promise<[FrameResultMap, number]> {
    const currentPassAlgorithms = firstSinglePassSegment(remainingAlgorithms);
    debug(`Doing an analysis pass with algorithms ${currentPassAlgorithms.map(a => a.name)}`);

    let tbn: number = undefined;
    if (!resultSet) {
        resultSet = {};
        for (const alg of allAlgorithms) {
            resultSet[alg.name] = [];
        }
    }

    const directoryPath = `${exchangeDirectory()}/${id}`;
    const framePromises: Promise<void>[] = [];
    const emptyResponse = chunkSuffix && allAlgorithms.some(a => a.dependencies.some(d => d.scope !== "currentFrame"));


    return new Promise<any>((resolve, reject) => {
        let width: number = undefined;
        let height: number = undefined;
        let frameNumber = -1;
        const decoder = fork(
            'api/helpers/decoder',
            [typeof source === "string" ? source : "pipe:", `${frameSkip}`, directoryPath],
            {silent: true}
        );
        createInterface(decoder.stdout)
        .on("line", line => {
            if (!width) {
                const items = line.split("\t");
                width = parseInt(items[0]);
                height = parseInt(items[1]);
                tbn = parseInt(items[2]);
                debug(`Video detected as having ${width}x${height} frames and tbn ${tbn}`);
            } else {
                ++frameNumber;
                framePromises.push(analyzeFrame(id, currentPassAlgorithms, directoryPath, resultSet, `${frameNumber}.dat`, line.trim(), frameNumber, width, height));
            }
        })
        .on("close", () => {
            debug(`Decoder has finished and there were ${frameNumber + 1} infos`);
            framePromises.push(analyzeFrame(id, currentPassAlgorithms, directoryPath, resultSet));
            resolve();
        });
    })
    .then(() => {
        debug("All frame analyses of current pass started");
        if (!emptyResponse) {
            return Promise.all(framePromises)
            .then(() => {
                debug("All frame analyses of current pass finished");
                if (remainingAlgorithms.length > currentPassAlgorithms.length) {
                    return doAnalysisPass(source, id, chunkSuffix, frameSkip, allAlgorithms, remainingAlgorithms.slice(currentPassAlgorithms.length), resultSet)
                } else {
                    if ((typeof source) !== "string") {
                        throw "Not supposed to try to build an output file when input has been piped to ffmpeg";
                    }
                    return Promise.resolve([resultSet, tbn]);
                }
            })
        } else {
            return Promise.resolve(null);
        }
    }, reason => Promise.reject(reason));
}

// It would make the code nicer if for each frame we returned a promise of a set of results...
// However, it turns out that instantiating a separate "hash" per each frame, and then merging those for each every pass is expensive, due to garbage collection
// So, instead use this single structure referenced in function calls, and have the promises be Promise<void>
function analyzeFrame(id: string, algorithms: IAlgorithmDocument[], directory: string, resultSet: FrameResultMap, frameFilename?: string, pts?: number, frameNumber?: number, width?: number, height?: number): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        const doneAlgorithms: Set<string> = new Set();
        let nextAlgorithm = 0;

        const frameIdentifier = (frameNumber === undefined) ? "eof" : frameNumber;

        function enqueueAlgorithm() {
            if (nextAlgorithm === algorithms.length) {
                return;
            }
            if (!algorithms[nextAlgorithm].dependencies.some(dep => algorithms.find(a => a.name === dep.name) && !doneAlgorithms.has(dep.name))) {
                const algorithm = algorithms[nextAlgorithm];
                ++nextAlgorithm;
                runAlgorithm(algorithm);
            }
        }

        function runAlgorithm(algorithm: IAlgorithmDocument) {
            let post_data = frameFilename ?
                `video_id=${id}&video_directory=${directory}&frame_number=${frameNumber}&frame_pts=${pts}&frame_filename=${frameFilename}&frame_width=${width}&frame_height=${height}`
              : `video_id=${id}&video_directory=${directory}&video_eof=true`;
            debug(`Requesting ${algorithm.host}:${algorithm.port} to run ${algorithm.name} with params ${post_data}`);
            request({
                host: algorithm.host,
                port: algorithm.port,
                path: "/analyze",
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Content-Length": Buffer.byteLength(post_data)
                }
            }, response => {
                if (response.statusCode === 200 || response.statusCode === 204) {
                    debug(`Response for ${algorithm.name} (${frameFilename || "eof"}) indicates success`);
                    doneAlgorithms.add(algorithm.name);
                    enqueueAlgorithm();
                } else {
                    return reject({detail: `Analyzer responded with ${response.statusCode}`});
                }
            })
            .on("error", err => reject({detail: err}))
            .end(post_data);
            post_data = null;

            enqueueAlgorithm();
        }

        enqueueAlgorithm();

        // we may get multiple indistinguishable events for the same file...
        const handledResults = new Set<string>();
        const watcher = watch(directory, (eventType, filename) => {
            if (eventType === "rename" || !(filename.startsWith(`${frameIdentifier}.`) && filename.endsWith(".json"))) {
                return;
            } else if (handledResults.has(filename)) {
                return;
            }
            handledResults.add(filename);

            if (handledResults.size === algorithms.length) {
                watcher.close();
                if (frameFilename) {
                    unlink(`${directory}/${frameFilename}`, err => {if (err) {console.error(err)}});
                }
            }
            debug(`Reading detected result file ${filename}`);
            readFile(`${directory}/${filename}`, "ascii", (err, data) => {
                if (err) {
                    watcher.close();
                    reject({detail: err});
                }
                const algorithmResultList = resultSet[filename.split(".")[1]];
                JSON.parse(data).forEach(r => algorithmResultList.push(r));
                if (handledResults.size === algorithms.length) {
                    resolve();
                }
            });
        });
    });
}

function buildAnalysisResponse(video: IVideoDocument, frameResults: FrameResultMap) {
    debug(`Storing analysis results`);
    return Analysis.insertMany(Object.keys(frameResults).map(algorithm => new Analysis({
        video: video._id,
        algorithm: video.analysisRuns.find(ar => ar.algorithm.name === algorithm).algorithm._id,
        results: frameResults[algorithm].sort((a, b) => a["timestamp"] - b["timestamp"])
    })))
    .then(analyses => {
        for (const analysis of analyses) {
            video.analysisRuns.find(ar => ar.algorithm._id === analysis.algorithm).analysis = analysis._id;
        }
        video.save();
        return analyses;
    });
}

function buildVideoResponse(id: string, originalVideoPath: string, contentType: string, frameResults: FrameResultMap, timebase: number) {
    return new Promise<string | FfmpegCommand>((resolve, reject) => {
        const subtitleTracks: {algorithm: string, content: Srt}[] = [];

        debug(`Building subtitle tracks for algorithms ${Object.keys(frameResults)}`);
        for (const algorithm of Object.keys(frameResults))  {
            const subtitle = new Srt();
            const ptsValues = new Set<number>();
            frameResults[algorithm].forEach(r => ptsValues.add(parseInt(r["timestamp"])));
            for (const pts of ptsValues) {
                subtitle.addLine(JSON.stringify(frameResults[algorithm].filter(r => r["timestamp"] === pts)), pts, timebase);
            }
            subtitleTracks.push({algorithm: algorithm, content: subtitle});
        }

        // Perhaps it would be possible to input the subtitle tracks via multiple pipes, but fluent-ffmpeg does not support this
        const subtitlePromises: Promise<string>[] = [];
        for (const subtitleTrack of subtitleTracks) {
            subtitlePromises.push(new Promise((resolve, reject) => {
                const filename = `${exchangeDirectory()}/${id}/${subtitleTrack.algorithm}.srt`;
                writeFile(filename, subtitleTrack.content.toString(), err => {
                    if (err) {
                        reject(err);
                    }
                    resolve(filename);
                })
            }));
        }

        Promise.all(subtitlePromises)
        .then(subtitleFiles => {
            let command = ffmpeg(originalVideoPath);
            subtitleFiles.forEach((subtitleFile, index) => {
                command = command.input(subtitleFile).addOutputOptions(`-map ${index + 1}`)
            })
            command = command
            .videoCodec("copy")
            .audioCodec("copy")
            .addOutputOptions("-map 0")
            .addOutputOptions("-scodec mov_text");
            if (contentType === "video/mp4") {
                debug("Running output via disk since MP4 requires seekable output");
                const outputFilePath = `${exchangeDirectory()}/${id}/output.mp4`;
                command.format("mp4")
                .output(outputFilePath)
                .on("end", () => {
                    resolve(outputFilePath);
                })
                .run();
            } else {
                debug("Piping multiplex output to response");
                resolve(command.format("mpegts"));
            }
        });
    })
}

export = {postVideoHeader, getVideoById, getVideoManifest, postVideoData};