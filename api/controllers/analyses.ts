import {Request, Response, NextFunction} from "express";
import {ParameterValue} from "sway";

import {IAnalysisDocument, Analysis} from "../../models/analysis";

function responseObject(analysis: IAnalysisDocument) {
    return {
        "id": analysis.id,
        "type": "analyses",
        "attributes": {
            "results": analysis.results,
        },
        "links": {
            "self": `/analyses/${analysis.id}`
        },
        "relationships": {
            "video": {
                "links": {
                    "related": `/videos/${analysis.video.toHexString()}`
                },
                "data": {
                    "id": analysis.video.toHexString(),
                    "type": "videos"
                }
            },
            "algorithm": {
                "links": {
                    "related": `/algorithms/${analysis.algorithm.toHexString()}`
                },
                "data": {
                    "id": analysis.algorithm.toHexString()
                }
            }
        }
    }
}

function getAnalysisById(req: Request, res: Response, next: NextFunction) {
    const idParam: ParameterValue = req["swagger"].params.analysisId;
    return Analysis.findById(idParam.value).then(
      doc => doc ? res.status(200).json({data: responseObject(doc)}) : res.sendStatus(404),
      err => res.status(500).json({"errors": [{"code": "DATABASE_ERROR", "detail": err}]})
    );
}

export = {responseObject, getAnalysisById}