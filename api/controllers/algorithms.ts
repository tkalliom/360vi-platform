import {Request, Response, NextFunction} from "express";
import {ParameterValue} from "sway";

import {IAlgorithmDocument, Algorithm, AlgorithmDependencyScope} from "../../models/algorithm";

function location(algorithm: IAlgorithmDocument) {
    if (!algorithm) {
        throw new RangeError("Trying to construct location of empty");
    } else if (!algorithm.id) {
        throw new RangeError("Trying to construct location of object without an id");
    } else {
        return `/algorithms/${algorithm.id}`;
    }
}

function responseObject(algorithm: IAlgorithmDocument) {
    return {
            "id": algorithm.id,
            "type": "algorithms",
            "attributes": algorithm.toObject({versionKey: false}),
            "links": {
                "self": location(algorithm)
            }
    }
}

function registerAlgorithm(req: Request, res: Response, next: NextFunction) {
    const host = req.body.host || req.header("X-Forwarded-For") || req.connection.remoteAddress.replace(/^::ffff:/, "");
    const port = parseInt(req.body.port) || 80;

    if (typeof req.body.current_frame_dependencies === "string") {
        req.body.current_frame_dependencies = [req.body.current_frame_dependencies];
    }
    if (typeof req.body.whole_video_dependencies === "string") {
        req.body.whole_video_dependencies = [req.body.whole_video_dependencies];
    }
    const reqParams: {name: string, version: number, host: string, port: number, dependencies: {name: string, minimumVersion: number, scope: AlgorithmDependencyScope}[]} = {
        name: req.body.name,
        version: req.body.version,
        host: host,
        port: port,
        dependencies: (req.body.current_frame_dependencies || []).map(dep => ({name: dep.split(":")[0], minimumVersion: parseInt(dep.split(":")[1]), scope: "currentFrame"}))
                      .concat((req.body.whole_video_dependencies || []).map(dep => ({name: dep.split(":")[0], minimumVersion: parseInt(dep.split(":")[1]), scope: "wholeVideo"})))
    };

    let existingDoc: IAlgorithmDocument = undefined;

    return Algorithm.findOne({name: reqParams.name, version: reqParams.version})
    .then(doc => {
        if (doc && doc.host === host && doc.port === port) {
            return Promise.reject({status: 409})
        } else if (doc) {
            existingDoc = doc;
        } else {
            return Promise.resolve(doc);
        }
    }, err => Promise.reject({detail: err}))
    .then(() => Algorithm.aggregate([
        {$match: {name: {$in: reqParams.dependencies.map(d => d.name)}}},
        {$group: {_id: "$name", maximumVersion: {$max: "$version"}}}
    ]))
    .then(aggregates => {
        for (const dep of reqParams.dependencies) {
            const algInfo = aggregates.find(a => a["_id"] === dep.name);
            if (!algInfo) {
                return Promise.reject({status: 422, code: "UNMET_DEPENDENCY", detail: `Unknown algorithm ${dep.name}`});
            } else if (algInfo["maximumVersion"] < dep.minimumVersion) {
                return Promise.reject({status: 422, code: "UNMET_DEPENDENCY", detail: `Maximum known version of ${dep.name} is ${algInfo["maximumVersion"]}`});
            }
        }

        if (reqParams.dependencies.length === 0) {
            delete reqParams.dependencies;
        }
        // Host/port is the only allowed change without version number bump
        const algorithm = existingDoc ? existingDoc.set({host: host, port: port}) : new Algorithm(reqParams);
        return algorithm.save();
    })
    .then(doc => (existingDoc ? res.status(200) : res.status(201).location(location(doc))).json({data: responseObject(doc)}))
    .catch(err => {
        const errorBody = {};
        if ("detail" in err) {
            errorBody["detail"] = err.detail;
        } else if ("message" in err) {
            errorBody["detail"] = err.message;
        }
        if ("code" in err) {
            errorBody["code"] = err.code;
        }
        return res.status(err.status || 500).json({errors: [errorBody]});
    });
}

function getAlgorithms(req: Request, res: Response, next: NextFunction) {
    return Algorithm.find({}).then(
        docs => res.status(200).json({data: docs.map(doc => responseObject(doc))}),
        err => res.status(500).json({errors: [{code: "DATABASE_ERROR", detail: err}]})
    )
}

function getAlgorithmById(req: Request, res: Response, next: NextFunction) {
    const idParam: ParameterValue = req["swagger"].params.algorithmId;
    return Algorithm.findById(idParam.value).then(
        doc => doc ? res.status(200).json({data: responseObject(doc)}) : res.sendStatus(404),
        err => res.status(500).json({errors: [{code: "DATABASE_ERROR", detail: err}]})
    )
}

export = {registerAlgorithm, getAlgorithms, getAlgorithmById}