FROM node:8-stretch

ENV PORT 80
EXPOSE 80
ENV MONGO_URI mongodb://mongodb/360vi-analysis-service
ENV DATA_EXCHANGE_DIRECTORY /tmp/360vi-analysis

# Buster ffmpeg has notably better HEVC performance
RUN echo 'deb http://deb.debian.org/debian buster main' > /etc/apt/sources.list.d/buster.list
RUN echo 'Package: *\nPin: release n=buster\nPin-Priority: -1' > /etc/apt/preferences.d/buster-inhibit
RUN echo 'Package: ffmpeg libavcodec* libavutil* libavresample* libswresample* libswscale* libavdevice* libavfilter* libavformat* libpostproc* libsdl2-2.0* libmp3lame* libwebpmux* libx265* libcdio* libva-* libva2 libass9 libfontconfig* libmysofa* libbluray* fontconfig-config\nPin: release n=buster\nPin-Priority: 500' > /etc/apt/preferences.d/buster-ffmpeg
RUN apt-get update && apt-get install -y ffmpeg

COPY . /app
WORKDIR /app
RUN npm install

CMD ["/usr/local/bin/npm", "start"]
